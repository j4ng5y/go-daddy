package contacts

type (
	Address struct {
		Address1   string `json:"address1"`
		Address2   string `json:"address2"`
		City       string `json:"city"`
		Country    string `json:"country"`
		PostalCode string `json:"postalCode"`
		State      string `json:"state"`
	}

	Contact struct {
		Address      `json:"addressMailing"`
		Email        string `json:"email"`
		JobTitle     string `json:"jobTitle"`
		NameFirst    string `json:"nameFirst"`
		NameMiddle   string `json:"nameMiddle"`
		NameLast     string `json:"nameLast"`
		Organization string `json:"organization"`
		Phone        string `json:"phone"`
	}
)
