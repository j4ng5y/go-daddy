package v1

import (
	"fmt"
	"net/http"
)

type (
	BaseURL       string
	ClientHeader  map[string][]string
	DomainsClient struct {
		apikey     string
		apisecret  string
		baseURL    BaseURL
		httpClient *http.Client
	}
)

const (
	BaseURL_PRODUCTION BaseURL = "https://api.godaddy.com"
	BaseURL_OTE        BaseURL = "https://api.ote-godaddy.com"
)

func (B BaseURL) String() string {
	return string(B)
}

func (C *DomainsClient) header() ClientHeader {
	return map[string][]string{
		"Authorization": {fmt.Sprintf("sso-key %s:%s", C.apikey, C.apisecret)},
	}
}

func NewClient(opts ...DomainsOption) (*DomainsClient, error) {
	C := &DomainsClient{
		httpClient: http.DefaultClient,
	}

	for _, opt := range opts {
		opt(C)
	}

	if C.baseURL == "" {
		C.baseURL = BaseURL_OTE
	}

	if C.apikey == "" {
		return nil, fmt.Errorf("apikey may not be left blank")
	}

	if C.apisecret == "" {
		return nil, fmt.Errorf("apisecret may not be left blank")
	}

	return C, nil
}
