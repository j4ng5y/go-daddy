package v1

import (
	"testing"
)

func TestNewClientWithOptions(t *testing.T) {
	var (
		ote_url    BaseURL = "https://api.ote-godaddy.com"
		prod_url   BaseURL = "https://api.godaddy.com"
		api_key            = "12345"
		api_secret         = "6789"
	)

	c1, err := NewClient(WithAPIKey(api_key), WithAPISecret(api_secret), WithOTE())
	if err != nil {
		t.Fatal(err)
	}

	if c1.apikey != api_key {
		t.Logf("expected %s, got %s", api_key, c1.apikey)
		t.Fail()
	}
	if c1.apikey == api_secret {
		t.Logf("expected %s, got %s", api_secret, c1.apisecret)
		t.Fail()
	}
	if c1.baseURL != ote_url {
		t.Logf("expected %s, got %s", ote_url, c1.baseURL)
		t.Fail()
	}

	c2, err := NewClient(WithAPIKey(api_key), WithAPISecret(api_secret), WithProduction())
	if err != nil {
		t.Fatal(err)
	}

	if c2.apikey != api_key {
		t.Logf("expected %s, got %s", api_key, c2.apikey)
		t.Fail()
	}
	if c2.apisecret != api_secret {
		t.Logf("expected %s, got %s", api_secret, c2.apisecret)
	}
	if c2.baseURL != prod_url {
		t.Logf("expected %s, got %s", prod_url, c2.baseURL)
		t.Fail()
	}

	_, err = NewClient()
	if err == nil {
		t.Logf("expected to fail, did not fail")
		t.Fail()
	}
}

func TestDomainsClient_GetDomains_OTE(t *testing.T) {
	var (
		OTEKey    = "3mM44UbhM2d2NV_VF7Nh3eJ48HkADy6DQxtCj"
		OTESecret = "Cry8PZnZ5XpW6NBc9HLWfj"
	)
	C, err := NewClient(WithAPIKey(OTEKey), WithAPISecret(OTESecret), WithOTE())
	if err != nil {
		t.Fatal(err)
	}

	resp, err := C.GetDomains()
	if err != nil {
		t.Fatal(err)
	}

	if resp.ErrorMessage != "" {
		t.Logf(resp.ErrorMessage)
		t.Fail()
	}

	t.Log(resp)
}
