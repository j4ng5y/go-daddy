package v1

type (
	DomainsOption           func(*DomainsClient)
	GetDomainsRequestOption func(*GetDomainsRequest)
)

func WithAPIKey(apikey string) DomainsOption {
	return func(C *DomainsClient) {
		C.apikey = apikey
	}
}

func WithAPISecret(apisecret string) DomainsOption {
	return func(C *DomainsClient) {
		C.apisecret = apisecret
	}
}

func WithOTE() DomainsOption {
	return func(C *DomainsClient) {
		C.baseURL = BaseURL_OTE
	}
}

func WithProduction() DomainsOption {
	return func(C *DomainsClient) {
		C.baseURL = BaseURL_PRODUCTION
	}
}

func WithXShopperID(id string) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.XShopperID = id
	}
}

func WithStatuses(statuses ...DomainStatus) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.Statuses = statuses
	}
}

func WithStatusGroups(groups ...DomainStatusGroup) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.StatusGroups = groups
	}
}

func WithLimit(limit int) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.Limit = limit
	}
}

func WithMarker(marker string) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.Marker = marker
	}
}

func WithIncludes(includes ...DomainIncludes) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.Includes = includes
	}
}

func WithModifiedDate(date string) GetDomainsRequestOption {
	return func(request *GetDomainsRequest) {
		request.ModifiedDate = date
	}
}
