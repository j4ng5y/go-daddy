package v1

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"gitlab.com/j4ng5y/go-daddy/contacts"
	"gitlab.com/j4ng5y/go-daddy/errors"
)

const (
	DomainStatus_ACTIVE                                      DomainStatus = "ACTIVE"
	DomainStatus_AWAITING_CLAIM_ACK                          DomainStatus = "AWAITING_CLAIM_ACK"
	DomainStatus_AWAITING_DOCUMENT_AFTER_TRANSFER            DomainStatus = "AWAITING_DOCUMENT_AFTER_TRANSFER"
	DomainStatus_AWAITING_DOCUMENT_AFTER_UPDATE_ACCOUNT      DomainStatus = "AWAITING_DOCUMENT_AFTER_UPDATE_ACCOUNT"
	DomainStatus_AWAITING_DOCUMENT_UPLOAD                    DomainStatus = "AWAITING_DOCUMENT_UPLOAD"
	DomainStatus_AWAITING_FAILED_TRANSFER_WHOIS_PRIVACY      DomainStatus = "AWAITING_FAILED_TRANSFER_WHOIS_PRIVACY"
	DomainStatus_AWAITING_PAYMENT                            DomainStatus = "AWAITING_PAYMENT"
	DomainStatus_AWAITING_RENEWAL_TRANSFER_IN_COMPLETE       DomainStatus = "AWAITING_RENEWAL_TRANSFER_IN_COMPLETE"
	DomainStatus_AWAITING_TRANSFER_IN_ACK                    DomainStatus = "AWAITING_TRANSFER_IN_ACK"
	DomainStatus_AWAITING_TRANSFER_IN_AUTH                   DomainStatus = "AWAITING_TRANSFER_IN_AUTH"
	DomainStatus_AWAITING_TRANSFER_IN_AUTO                   DomainStatus = "AWAITING_TRANSFER_IN_AUTO"
	DomainStatus_AWAITING_TRANSFER_IN_WHOIS                  DomainStatus = "AWAITING_TRANSFER_IN_WHOIS"
	DomainStatus_AWAITING_TRANSFER_IN_WHOIS_FIX              DomainStatus = "AWAITING_TRANSFER_IN_WHOIS_FIX"
	DomainStatus_AWAITING_VERIFICATION_ICANN                 DomainStatus = "AWAITING_VERIFICATION_ICANN"
	DomainStatus_AWAITING_VERIFICATION_ICANN_MANUAL          DomainStatus = "AWAITING_VERIFICATION_ICANN_MANUAL"
	DomainStatus_CANCELLED                                   DomainStatus = "CANCELLED"
	DomainStatus_CANCELLED_HELD                              DomainStatus = "CANCELLED_HELD"
	DomainStatus_CANCELLED_REDEEMABLE                        DomainStatus = "CANCELLED_REDEEMABLE"
	DomainStatus_CANCELLED_TRANSFER                          DomainStatus = "CANCELLED_TRANSFER"
	DomainStatus_CONFISCATED                                 DomainStatus = "CONFISCATED"
	DomainStatus_DISABLED_SPECIAL                            DomainStatus = "DISABLED_SPECIAL"
	DomainStatus_EXCLUDED_INVALID_CLAIM_FIREHOSE             DomainStatus = "EXCLUDED_INVALID_CLAIM_FIREHOSE"
	DomainStatus_EXPIRED_REASSIGNED                          DomainStatus = "EXPIRED_REASSIGNED"
	DomainStatus_FAILED_BACKORDER_CAPTURE                    DomainStatus = "FAILED_BACKORDER_CAPTURE"
	DomainStatus_FAILED_DROP_IMMEDIATE_THEN_ADD              DomainStatus = "FAILED_DROP_IMMEDIATE_THEN_ADD"
	DomainStatus_FAILED_PRE_REGISTRATION                     DomainStatus = "FAILED_PRE_REGISTRATION"
	DomainStatus_FAILED_REDEMPTION                           DomainStatus = "FAILED_REDEMPTION"
	DomainStatus_FAILED_REDEMPTION_REPORT                    DomainStatus = "FAILED_REDEMPTION_REPORT"
	DomainStatus_FAILED_REGISTRATION                         DomainStatus = "FAILED_REGISTRATION"
	DomainStatus_FAILED_REGISTRATION_FIREHOSE                DomainStatus = "FAILED_REGISTRATION_FIREHOSE"
	DomainStatus_FAILED_RESTORATION_REDEMPTION_MOCK          DomainStatus = "FAILED_RESTORATION_REDEMPTION_MOCK"
	DomainStatus_FAILED_SETUP                                DomainStatus = "FAILED_SETUP"
	DomainStatus_FAILED_TRANSFER_IN                          DomainStatus = "FAILED_TRANSFER_IN"
	DomainStatus_FAILED_TRANSFER_IN_BAD_STATUS               DomainStatus = "FAILED_TRANSFER_IN_BAD_STATUS"
	DomainStatus_FAILED_TRANSFER_IN_REGISTRY                 DomainStatus = "FAILED_TRANSFER_IN_REGISTRY"
	DomainStatus_HELD_COURT_ORDERED                          DomainStatus = "HELD_COURT_ORDERED"
	DomainStatus_HELD_DISPUTED                               DomainStatus = "HELD_DISPUTED"
	DomainStatus_HELD_EXPIRATION_PROTECTION                  DomainStatus = "HELD_EXPIRATION_PROTECTION"
	DomainStatus_HELD_EXPIRED_REDEMPTION_MOCK                DomainStatus = "HELD_EXPIRED_REDEMPTION_MOCK"
	DomainStatus_HELD_REGISTRAR_ADD                          DomainStatus = "HELD_REGISTRAR_ADD"
	DomainStatus_HELD_REGISTRAR_REMOVE                       DomainStatus = "HELD_REGISTRAR_REMOVE"
	DomainStatus_HELD_SHOPPER                                DomainStatus = "HELD_SHOPPER"
	DomainStatus_HELD_TEMPORARY                              DomainStatus = "HELD_TEMPORARY"
	DomainStatus_LOCKED_ABUSE                                DomainStatus = "LOCKED_ABUSE"
	DomainStatus_LOCKED_COPYRIGHT                            DomainStatus = "LOCKED_COPYRIGHT"
	DomainStatus_LOCKED_REGISTRY                             DomainStatus = "LOCKED_REGISTRY"
	DomainStatus_LOCKED_SUPER                                DomainStatus = "LOCKED_SUPER"
	DomainStatus_PARKED_AND_HELD                             DomainStatus = "PARKED_AND_HELD"
	DomainStatus_PARKED_EXPIRED                              DomainStatus = "PARKED_EXPIRED"
	DomainStatus_PARKED_VERIFICATION_ICANN                   DomainStatus = "PARKED_VERIFICATION_ICANN"
	DomainStatus_PENDING_ABORT_CANCEL_SETUP                  DomainStatus = "PENDING_ABORT_CANCEL_SETUP"
	DomainStatus_PENDING_AGREEMENT_PRE_REGISTRATION          DomainStatus = "PENDING_AGREEMENT_PRE_REGISTRATION"
	DomainStatus_PENDING_APPLY_RENEWAL_CREDITS               DomainStatus = "PENDING_APPLY_RENEWAL_CREDITS"
	DomainStatus_PENDING_BACKORDER_CAPTURE                   DomainStatus = "PENDING_BACKORDER_CAPTURE"
	DomainStatus_PENDING_BLOCKED_REGISTRY                    DomainStatus = "PENDING_BLOCKED_REGISTRY"
	DomainStatus_PENDING_CANCEL_REGISTRANT_PROFILE           DomainStatus = "PENDING_CANCEL_REGISTRANT_PROFILE"
	DomainStatus_PENDING_COMPLETE_REDEMPTION_WITHOUT_RECEIPT DomainStatus = "PENDING_COMPLETE_REDEMPTION_WITHOUT_RECEIPT"
	DomainStatus_PENDING_COMPLETE_REGISTRANT_PROFILE         DomainStatus = "PENDING_COMPLETE_REGISTRANT_PROFILE"
	DomainStatus_PENDING_COO                                 DomainStatus = "PENDING_COO"
	DomainStatus_PENDING_COO_COMPLETE                        DomainStatus = "PENDING_COO_COMPLETE"
	DomainStatus_PENDING_DNS                                 DomainStatus = "PENDING_DNS"
	DomainStatus_PENDING_DNS_ACTIVE                          DomainStatus = "PENDING_DNS_ACTIVE"
	DomainStatus_PENDING_DNS_INACTIVE                        DomainStatus = "PENDING_DNS_INACTIVE"
	DomainStatus_PENDING_DOCUMENT_VALIDATION                 DomainStatus = "PENDING_DOCUMENT_VALIDATION"
	DomainStatus_PENDING_DOCUMENT_VERIFICATION               DomainStatus = "PENDING_DOCUMENT_VERIFICATION"
	DomainStatus_PENDING_DROP_IMMEDIATE                      DomainStatus = "PENDING_DROP_IMMEDIATE"
	DomainStatus_PENDING_DROP_IMMEDIATE_THEN_ADD             DomainStatus = "PENDING_DROP_IMMEDIATE_THEN_ADD"
	DomainStatus_PENDING_EPP_CREATE                          DomainStatus = "PENDING_EPP_CREATE"
	DomainStatus_PENDING_EPP_DELETE                          DomainStatus = "PENDING_EPP_DELETE"
	DomainStatus_PENDING_EPP_UPDATE                          DomainStatus = "PENDING_EPP_UPDATE"
	DomainStatus_PENDING_ESCALATION_REGISTRY                 DomainStatus = "PENDING_ESCALATION_REGISTRY"
	DomainStatus_PENDING_EXPIRATION                          DomainStatus = "PENDING_EXPIRATION"
	DomainStatus_PENDING_EXPIRATION_RESPONSE                 DomainStatus = "PENDING_EXPIRATION_RESPONSE"
	DomainStatus_PENDING_EXPIRATION_SYNC                     DomainStatus = "PENDING_EXPIRATION_SYNC"
	DomainStatus_PENDING_EXPIRED_REASSIGNMENT                DomainStatus = "PENDING_EXPIRED_REASSIGNMENT"
	DomainStatus_PENDING_EXPIRE_AUTO_ADD                     DomainStatus = "PENDING_EXPIRE_AUTO_ADD"
	DomainStatus_PENDING_EXTEND_REGISTRANT_PROFILE           DomainStatus = "PENDING_EXTEND_REGISTRANT_PROFILE"
	DomainStatus_PENDING_FAILED_COO                          DomainStatus = "PENDING_FAILED_COO"
	DomainStatus_PENDING_FAILED_EPP_CREATE                   DomainStatus = "PENDING_FAILED_EPP_CREATE"
	DomainStatus_PENDING_FAILED_HELD                         DomainStatus = "PENDING_FAILED_HELD"
	DomainStatus_PENDING_FAILED_PURCHASE_PREMIUM             DomainStatus = "PENDING_FAILED_PURCHASE_PREMIUM"
	DomainStatus_PENDING_FAILED_RECONCILE_FIREHOSE           DomainStatus = "PENDING_FAILED_RECONCILE_FIREHOSE"
	DomainStatus_PENDING_FAILED_REDEMPTION_WITHOUT_RECEIPT   DomainStatus = "PENDING_FAILED_REDEMPTION_WITHOUT_RECEIPT"
	DomainStatus_PENDING_FAILED_RELEASE_PREMIUM              DomainStatus = "PENDING_FAILED_RELEASE_PREMIUM"
	DomainStatus_PENDING_FAILED_RENEW_EXPIRATION_PROTECTION  DomainStatus = "PENDING_FAILED_RENEW_EXPIRATION_PROTECTION"
	DomainStatus_PENDING_FAILED_RESERVE_PREMIUM              DomainStatus = "PENDING_FAILED_RESERVE_PREMIUM"
	DomainStatus_PENDING_FAILED_SUBMIT_FIREHOSE              DomainStatus = "PENDING_FAILED_SUBMIT_FIREHOSE"
	DomainStatus_PENDING_FAILED_TRANSFER_ACK_PREMIUM         DomainStatus = "PENDING_FAILED_TRANSFER_ACK_PREMIUM"
	DomainStatus_PENDING_FAILED_TRANSFER_IN_ACK_PREMIUM      DomainStatus = "PENDING_FAILED_TRANSFER_IN_ACK_PREMIUM"
	DomainStatus_PENDING_FAILED_TRANSFER_IN_PREMIUM          DomainStatus = "PENDING_FAILED_TRANSFER_IN_PREMIUM"
	DomainStatus_PENDING_FAILED_TRANSFER_PREMIUM             DomainStatus = "PENDING_FAILED_TRANSFER_PREMIUM"
	DomainStatus_PENDING_FAILED_TRANSFER_SUBMIT_PREMIUM      DomainStatus = "PENDING_FAILED_TRANSFER_SUBMIT_PREMIUM"
	DomainStatus_PENDING_FAILED_UNLOCK_PREMIUM               DomainStatus = "PENDING_FAILED_UNLOCK_PREMIUM"
	DomainStatus_PENDING_FAILED_UPDATE_API                   DomainStatus = "PENDING_FAILED_UPDATE_API"
	DomainStatus_PENDING_FRAUD_VERIFICATION                  DomainStatus = "PENDING_FRAUD_VERIFICATION"
	DomainStatus_PENDING_FRAUD_VERIFIED                      DomainStatus = "PENDING_FRAUD_VERIFIED"
	DomainStatus_PENDING_GET_CONTACTS                        DomainStatus = "PENDING_GET_CONTACTS"
	DomainStatus_PENDING_GET_HOSTS                           DomainStatus = "PENDING_GET_HOSTS"
	DomainStatus_PENDING_GET_NAME_SERVERS                    DomainStatus = "PENDING_GET_NAME_SERVERS"
	DomainStatus_PENDING_GET_STATUS                          DomainStatus = "PENDING_GET_STATUS"
	DomainStatus_PENDING_HOLD_ESCROW                         DomainStatus = "PENDING_HOLD_ESCROW"
	DomainStatus_PENDING_HOLD_REDEMPTION                     DomainStatus = "PENDING_HOLD_REDEMPTION"
	DomainStatus_PENDING_LOCK_CLIENT_REMOVE                  DomainStatus = "PENDING_LOCK_CLIENT_REMOVE"
	DomainStatus_PENDING_LOCK_DATA_QUALITY                   DomainStatus = "PENDING_LOCK_DATA_QUALITY"
	DomainStatus_PENDING_LOCK_THEN_HOLD_REDEMPTION           DomainStatus = "PENDING_LOCK_THEN_HOLD_REDEMPTION"
	DomainStatus_PENDING_PARKING_DETERMINATION               DomainStatus = "PENDING_PARKING_DETERMINATION"
	DomainStatus_PENDING_PARK_INVALID_WHOIS                  DomainStatus = "PENDING_PARK_INVALID_WHOIS"
	DomainStatus_PENDING_PARK_INVALID_WHOIS_REMOVAL          DomainStatus = "PENDING_PARK_INVALID_WHOIS_REMOVAL"
	DomainStatus_PENDING_PURCHASE_PREMIUM                    DomainStatus = "PENDING_PURCHASE_PREMIUM"
	DomainStatus_PENDING_RECONCILE                           DomainStatus = "PENDING_RECONCILE"
	DomainStatus_PENDING_RECONCILE_FIREHOSE                  DomainStatus = "PENDING_RECONCILE_FIREHOSE"
	DomainStatus_PENDING_REDEMPTION                          DomainStatus = "PENDING_REDEMPTION"
	DomainStatus_PENDING_REDEMPTION_REPORT                   DomainStatus = "PENDING_REDEMPTION_REPORT"
	DomainStatus_PENDING_REDEMPTION_REPORT_COMPLETE          DomainStatus = "PENDING_REDEMPTION_REPORT_COMPLETE"
	DomainStatus_PENDING_REDEMPTION_REPORT_SUBMITTED         DomainStatus = "PENDING_REDEMPTION_REPORT_SUBMITTED"
	DomainStatus_PENDING_REDEMPTION_WITHOUT_RECEIPT          DomainStatus = "PENDING_REDEMPTION_WITHOUT_RECEIPT"
	DomainStatus_PENDING_REDEMPTION_WITHOUT_RECEIPT_MOCK     DomainStatus = "PENDING_REDEMPTION_WITHOUT_RECEIPT_MOCK"
	DomainStatus_PENDING_RELEASE_PREMIUM                     DomainStatus = "PENDING_RELEASE_PREMIUM"
	DomainStatus_PENDING_REMOVAL                             DomainStatus = "PENDING_REMOVAL"
	DomainStatus_PENDING_REMOVAL_HELD                        DomainStatus = "PENDING_REMOVAL_HELD"
	DomainStatus_PENDING_REMOVAL_PARKED                      DomainStatus = "PENDING_REMOVAL_PARKED"
	DomainStatus_PENDING_REMOVAL_UNPARK                      DomainStatus = "PENDING_REMOVAL_UNPARK"
	DomainStatus_PENDING_RENEWAL                             DomainStatus = "PENDING_RENEWAL"
	DomainStatus_PENDING_RENEW_EXPIRATION_PROTECTION         DomainStatus = "PENDING_RENEW_EXPIRATION_PROTECTION"
	DomainStatus_PENDING_RENEW_INFINITE                      DomainStatus = "PENDING_RENEW_INFINITE"
	DomainStatus_PENDING_RENEW_LOCKED                        DomainStatus = "PENDING_RENEW_LOCKED"
	DomainStatus_PENDING_RENEW_WITHOUT_RECEIPT               DomainStatus = "PENDING_RENEW_WITHOUT_RECEIPT"
	DomainStatus_PENDING_REPORT_REDEMPTION_WITHOUT_RECEIPT   DomainStatus = "PENDING_REPORT_REDEMPTION_WITHOUT_RECEIPT"
	DomainStatus_PENDING_RESERVE_PREMIUM                     DomainStatus = "PENDING_RESERVE_PREMIUM"
	DomainStatus_PENDING_RESET_VERIFICATION_ICANN            DomainStatus = "PENDING_RESET_VERIFICATION_ICANN"
	DomainStatus_PENDING_RESPONSE_FIREHOSE                   DomainStatus = "PENDING_RESPONSE_FIREHOSE"
	DomainStatus_PENDING_RESTORATION                         DomainStatus = "PENDING_RESTORATION"
	DomainStatus_PENDING_RESTORATION_INACTIVE                DomainStatus = "PENDING_RESTORATION_INACTIVE"
	DomainStatus_PENDING_RESTORATION_REDEMPTION_MOCK         DomainStatus = "PENDING_RESTORATION_REDEMPTION_MOCK"
	DomainStatus_PENDING_RETRY_EPP_CREATE                    DomainStatus = "PENDING_RETRY_EPP_CREATE"
	DomainStatus_PENDING_RETRY_HELD                          DomainStatus = "PENDING_RETRY_HELD"
	DomainStatus_PENDING_SEND_AUTH_CODE                      DomainStatus = "PENDING_SEND_AUTH_CODE"
	DomainStatus_PENDING_SETUP                               DomainStatus = "PENDING_SETUP"
	DomainStatus_PENDING_SETUP_ABANDON                       DomainStatus = "PENDING_SETUP_ABANDON"
	DomainStatus_PENDING_SETUP_AGREEMENT_LANDRUSH            DomainStatus = "PENDING_SETUP_AGREEMENT_LANDRUSH"
	DomainStatus_PENDING_SETUP_AGREEMENT_SUNRISE2_A          DomainStatus = "PENDING_SETUP_AGREEMENT_SUNRISE2_A"
	DomainStatus_PENDING_SETUP_AGREEMENT_SUNRISE2_B          DomainStatus = "PENDING_SETUP_AGREEMENT_SUNRISE2_B"
	DomainStatus_PENDING_SETUP_AGREEMENT_SUNRISE2_C          DomainStatus = "PENDING_SETUP_AGREEMENT_SUNRISE2_C"
	DomainStatus_PENDING_SETUP_AUTH                          DomainStatus = "PENDING_SETUP_AUTH"
	DomainStatus_PENDING_SETUP_DNS                           DomainStatus = "PENDING_SETUP_DNS"
	DomainStatus_PENDING_SETUP_FAILED                        DomainStatus = "PENDING_SETUP_FAILED"
	DomainStatus_PENDING_SETUP_REVIEW                        DomainStatus = "PENDING_SETUP_REVIEW"
	DomainStatus_PENDING_SETUP_SUNRISE                       DomainStatus = "PENDING_SETUP_SUNRISE"
	DomainStatus_PENDING_SETUP_SUNRISE_PRE                   DomainStatus = "PENDING_SETUP_SUNRISE_PRE"
	DomainStatus_PENDING_SETUP_SUNRISE_RESPONSE              DomainStatus = "PENDING_SETUP_SUNRISE_RESPONSE"
	DomainStatus_PENDING_SUBMIT_FAILURE                      DomainStatus = "PENDING_SUBMIT_FAILURE"
	DomainStatus_PENDING_SUBMIT_FIREHOSE                     DomainStatus = "PENDING_SUBMIT_FIREHOSE"
	DomainStatus_PENDING_SUBMIT_HOLD_FIREHOSE                DomainStatus = "PENDING_SUBMIT_HOLD_FIREHOSE"
	DomainStatus_PENDING_SUBMIT_HOLD_LANDRUSH                DomainStatus = "PENDING_SUBMIT_HOLD_LANDRUSH"
	DomainStatus_PENDING_SUBMIT_HOLD_SUNRISE                 DomainStatus = "PENDING_SUBMIT_HOLD_SUNRISE"
	DomainStatus_PENDING_SUBMIT_LANDRUSH                     DomainStatus = "PENDING_SUBMIT_LANDRUSH"
	DomainStatus_PENDING_SUBMIT_RESPONSE_FIREHOSE            DomainStatus = "PENDING_SUBMIT_RESPONSE_FIREHOSE"
	DomainStatus_PENDING_SUBMIT_RESPONSE_LANDRUSH            DomainStatus = "PENDING_SUBMIT_RESPONSE_LANDRUSH"
	DomainStatus_PENDING_SUBMIT_RESPONSE_SUNRISE             DomainStatus = "PENDING_SUBMIT_RESPONSE_SUNRISE"
	DomainStatus_PENDING_SUBMIT_SUCCESS_FIREHOSE             DomainStatus = "PENDING_SUBMIT_SUCCESS_FIREHOSE"
	DomainStatus_PENDING_SUBMIT_SUCCESS_LANDRUSH             DomainStatus = "PENDING_SUBMIT_SUCCESS_LANDRUSH"
	DomainStatus_PENDING_SUBMIT_SUCCESS_SUNRISE              DomainStatus = "PENDING_SUBMIT_SUCCESS_SUNRISE"
	DomainStatus_PENDING_SUBMIT_SUNRISE                      DomainStatus = "PENDING_SUBMIT_SUNRISE"
	DomainStatus_PENDING_SUBMIT_WAITING_LANDRUSH             DomainStatus = "PENDING_SUBMIT_WAITING_LANDRUSH"
	DomainStatus_PENDING_SUCCESS_PRE_REGISTRATION            DomainStatus = "PENDING_SUCCESS_PRE_REGISTRATION"
	DomainStatus_PENDING_SUSPENDED_DATA_QUALITY              DomainStatus = "PENDING_SUSPENDED_DATA_QUALITY"
	DomainStatus_PENDING_TRANSFER_ACK_PREMIUM                DomainStatus = "PENDING_TRANSFER_ACK_PREMIUM"
	DomainStatus_PENDING_TRANSFER_IN                         DomainStatus = "PENDING_TRANSFER_IN"
	DomainStatus_PENDING_TRANSFER_IN_ACK                     DomainStatus = "PENDING_TRANSFER_IN_ACK"
	DomainStatus_PENDING_TRANSFER_IN_ACK_PREMIUM             DomainStatus = "PENDING_TRANSFER_IN_ACK_PREMIUM"
	DomainStatus_PENDING_TRANSFER_IN_BAD_REGISTRANT          DomainStatus = "PENDING_TRANSFER_IN_BAD_REGISTRANT"
	DomainStatus_PENDING_TRANSFER_IN_CANCEL                  DomainStatus = "PENDING_TRANSFER_IN_CANCEL"
	DomainStatus_PENDING_TRANSFER_IN_CANCEL_REGISTRY         DomainStatus = "PENDING_TRANSFER_IN_CANCEL_REGISTRY"
	DomainStatus_PENDING_TRANSFER_IN_COMPLETE_ACK            DomainStatus = "PENDING_TRANSFER_IN_COMPLETE_ACK"
	DomainStatus_PENDING_TRANSFER_IN_DELETE                  DomainStatus = "PENDING_TRANSFER_IN_DELETE"
	DomainStatus_PENDING_TRANSFER_IN_LOCK                    DomainStatus = "PENDING_TRANSFER_IN_LOCK"
	DomainStatus_PENDING_TRANSFER_IN_NACK                    DomainStatus = "PENDING_TRANSFER_IN_NACK"
	DomainStatus_PENDING_TRANSFER_IN_NOTIFICATION            DomainStatus = "PENDING_TRANSFER_IN_NOTIFICATION"
	DomainStatus_PENDING_TRANSFER_IN_PREMIUM                 DomainStatus = "PENDING_TRANSFER_IN_PREMIUM"
	DomainStatus_PENDING_TRANSFER_IN_RELEASE                 DomainStatus = "PENDING_TRANSFER_IN_RELEASE"
	DomainStatus_PENDING_TRANSFER_IN_RESPONSE                DomainStatus = "PENDING_TRANSFER_IN_RESPONSE"
	DomainStatus_PENDING_TRANSFER_IN_UNDERAGE                DomainStatus = "PENDING_TRANSFER_IN_UNDERAGE"
	DomainStatus_PENDING_TRANSFER_OUT                        DomainStatus = "PENDING_TRANSFER_OUT"
	DomainStatus_PENDING_TRANSFER_OUT_ACK                    DomainStatus = "PENDING_TRANSFER_OUT_ACK"
	DomainStatus_PENDING_TRANSFER_OUT_NACK                   DomainStatus = "PENDING_TRANSFER_OUT_NACK"
	DomainStatus_PENDING_TRANSFER_OUT_PREMIUM                DomainStatus = "PENDING_TRANSFER_OUT_PREMIUM"
	DomainStatus_PENDING_TRANSFER_OUT_UNDERAGE               DomainStatus = "PENDING_TRANSFER_OUT_UNDERAGE"
	DomainStatus_PENDING_TRANSFER_OUT_VALIDATION             DomainStatus = "PENDING_TRANSFER_OUT_VALIDATION"
	DomainStatus_PENDING_TRANSFER_PREMIUM                    DomainStatus = "PENDING_TRANSFER_PREMIUM"
	DomainStatus_PENDING_TRANSFER_PREMUIM                    DomainStatus = "PENDING_TRANSFER_PREMUIM"
	DomainStatus_PENDING_TRANSFER_SUBMIT_PREMIUM             DomainStatus = "PENDING_TRANSFER_SUBMIT_PREMIUM"
	DomainStatus_PENDING_UNLOCK_DATA_QUALITY                 DomainStatus = "PENDING_UNLOCK_DATA_QUALITY"
	DomainStatus_PENDING_UNLOCK_PREMIUM                      DomainStatus = "PENDING_UNLOCK_PREMIUM"
	DomainStatus_PENDING_UPDATE                              DomainStatus = "PENDING_UPDATE"
	DomainStatus_PENDING_UPDATED_REGISTRANT_DATA_QUALITY     DomainStatus = "PENDING_UPDATED_REGISTRANT_DATA_QUALITY"
	DomainStatus_PENDING_UPDATE_ACCOUNT                      DomainStatus = "PENDING_UPDATE_ACCOUNT"
	DomainStatus_PENDING_UPDATE_API                          DomainStatus = "PENDING_UPDATE_API"
	DomainStatus_PENDING_UPDATE_API_RESPONSE                 DomainStatus = "PENDING_UPDATE_API_RESPONSE"
	DomainStatus_PENDING_UPDATE_AUTH                         DomainStatus = "PENDING_UPDATE_AUTH"
	DomainStatus_PENDING_UPDATE_CONTACTS                     DomainStatus = "PENDING_UPDATE_CONTACTS"
	DomainStatus_PENDING_UPDATE_CONTACTS_PRIVACY             DomainStatus = "PENDING_UPDATE_CONTACTS_PRIVACY"
	DomainStatus_PENDING_UPDATE_DNS                          DomainStatus = "PENDING_UPDATE_DNS"
	DomainStatus_PENDING_UPDATE_DNS_SECURITY                 DomainStatus = "PENDING_UPDATE_DNS_SECURITY"
	DomainStatus_PENDING_UPDATE_ELIGIBILITY                  DomainStatus = "PENDING_UPDATE_ELIGIBILITY"
	DomainStatus_PENDING_UPDATE_EPP_CONTACTS                 DomainStatus = "PENDING_UPDATE_EPP_CONTACTS"
	DomainStatus_PENDING_UPDATE_MEMBERSHIP                   DomainStatus = "PENDING_UPDATE_MEMBERSHIP"
	DomainStatus_PENDING_UPDATE_OWNERSHIP                    DomainStatus = "PENDING_UPDATE_OWNERSHIP"
	DomainStatus_PENDING_UPDATE_OWNERSHIP_AUTH_AUCTION       DomainStatus = "PENDING_UPDATE_OWNERSHIP_AUTH_AUCTION"
	DomainStatus_PENDING_UPDATE_OWNERSHIP_HELD               DomainStatus = "PENDING_UPDATE_OWNERSHIP_HELD"
	DomainStatus_PENDING_UPDATE_REGISTRANT                   DomainStatus = "PENDING_UPDATE_REGISTRANT"
	DomainStatus_PENDING_UPDATE_REPO                         DomainStatus = "PENDING_UPDATE_REPO"
	DomainStatus_PENDING_VALIDATION_DATA_QUALITY             DomainStatus = "PENDING_VALIDATION_DATA_QUALITY"
	DomainStatus_PENDING_VERIFICATION_FRAUD                  DomainStatus = "PENDING_VERIFICATION_FRAUD"
	DomainStatus_PENDING_VERIFICATION_STATUS                 DomainStatus = "PENDING_VERIFICATION_STATUS"
	DomainStatus_PENDING_VERIFY_REGISTRANT_DATA_QUALITY      DomainStatus = "PENDING_VERIFY_REGISTRANT_DATA_QUALITY"
	DomainStatus_RESERVED                                    DomainStatus = "RESERVED"
	DomainStatus_RESERVED_PREMIUM                            DomainStatus = "RESERVED_PREMIUM"
	DomainStatus_REVERTED                                    DomainStatus = "REVERTED"
	DomainStatus_SUSPENDED_VERIFICATION_ICANN                DomainStatus = "SUSPENDED_VERIFICATION_ICANN"
	DomainStatus_TRANSFERRED_OUT                             DomainStatus = "TRANSFERRED_OUT"
	DomainStatus_UNLOCKED_ABUSE                              DomainStatus = "UNLOCKED_ABUSE"
	DomainStatus_UNLOCKED_SUPER                              DomainStatus = "UNLOCKED_SUPER"
	DomainStatus_UNPARKED_AND_UNHELD                         DomainStatus = "UNPARKED_AND_UNHELD"
	DomainStatus_UPDATED_OWNERSHIP                           DomainStatus = "UPDATED_OWNERSHIP"
	DomainStatus_UPDATED_OWNERSHIP_HELD                      DomainStatus = "UPDATED_OWNERSHIP_HELD"

	DomainStatusGroup_INACTIVE           DomainStatusGroup = "INACTIVE"
	DomainStatusGroup_PRE_REGISTRATION   DomainStatusGroup = "PRE_REGISTRATION"
	DomainStatusGroup_REDEMPTION         DomainStatusGroup = "REDEMPTION"
	DomainStatusGroup_RENEWABLE          DomainStatusGroup = "RENEWABLE"
	DomainStatusGroup_VERIFICATION_ICANN DomainStatusGroup = "VERIFICATION_ICANN"
	DomainStatusGroup_VISIBLE            DomainStatusGroup = "VISIBLE"

	DomainIncludes_AUTHCODE     DomainIncludes = "authCode"
	DomainIncludes_CONTACTS     DomainIncludes = "contacts"
	DomainIncludes_NAME_SERVERS DomainIncludes = "nameServers"
)

type (
	DomainStatus      string
	DomainStatusGroup string
	DomainIncludes    string

	GetDomainsRequest struct {
		XShopperID   string              // Shopper ID whose domains are to be retrieved
		Statuses     []DomainStatus      // Only include results with 'status' value in the specified set
		StatusGroups []DomainStatusGroup // Only include results with 'status' value in the specified groups
		Limit        int                 // Maximum number of domains to return
		Marker       string              // Marker Domain to use as the offset in results
		Includes     []DomainIncludes    // Optional details to be included in the response
		ModifiedDate string              // Only include results that have been modified since the specified date
	}
	GetDomainsResponse struct {
		// Erroneous Request:
		ErrorCode          string                 `json:"code"`
		ErrorFields        []errors.ErrorResponse `json:"fields"`
		ErrorMessage       string                 `json:"message"`
		ErrorRetryAfterSec int                    `json:"retryAfterSec"` // Only returned on rate limited requests

		// Successful Request:
		Domains []*GetDomainsDomain
	}
	GetDomainsDomain struct {
		AuthCode               string           `json:"authCode"`
		ContactAdmin           contacts.Contact `json:"contactAdmin"`
		ContactBilling         contacts.Contact `json:"contactBilling"`
		ContactRegistrant      contacts.Contact `json:"contactRegistrant"`
		ContactTech            contacts.Contact `json:"contactTech"`
		CreatedAt              string           `json:"createdAt"`
		DeletedAt              string           `json:"deletedAt"`
		TransferAwayEligibleAt string           `json:"transferAwayEligibleAt"`
		Domain                 string           `json:"domain"`
		DomainID               int              `json:"domainID"`
		ExpirationProtected    bool             `json:"expirationProtected"`
		Expires                string           `json:"expires"`
		ExposeWhois            bool             `json:"exposeWhois"`
		HoldRegistrar          bool             `json:"holdRegistrar"`
		Locked                 bool             `json:"locked"`
		NameServers            []string         `json:"nameServers"`
		Privacy                bool             `json:"privacy"`
		RenewAuto              bool             `json:"renewAuto"`
		RenewDeadline          string           `json:"renewDeadline"`
		Renewable              bool             `json:"renewable"`
		Status                 string           `json:"status"`
		TransferProtected      bool             `json:"transferProtected"`
	}
)

func (d DomainStatus) String() string {
	return string(d)
}

func (d DomainStatusGroup) String() string {
	return string(d)
}

func (d DomainIncludes) String() string {
	return string(d)
}

func (req *GetDomainsRequest) xShopperID() (bool, []string) {
	if req.XShopperID != "" {
		return true, []string{req.XShopperID}
	} else {
		return false, nil
	}
}

func (req *GetDomainsRequest) statuses() (bool, string) {
	if req.Statuses == nil {
		return false, ""
	} else if len(req.Statuses) == 0 {
		return false, ""
	} else {
		var ss []string
		for _, s := range req.Statuses {
			ss = append(ss, s.String())
		}
		return true, fmt.Sprintf("statuses=%s", strings.Join(ss, ","))
	}
}

func (req *GetDomainsRequest) statusGroups() (bool, string) {
	if req.StatusGroups == nil {
		return false, ""
	} else if len(req.StatusGroups) == 0 {
		return false, ""
	} else {
		var ss []string
		for _, s := range req.StatusGroups {
			ss = append(ss, s.String())
		}
		return true, fmt.Sprintf("statusGroups=%s", strings.Join(ss, ","))
	}
}

func (req *GetDomainsRequest) limit() (bool, string) {
	if req.Limit > 0 {
		return true, fmt.Sprintf("limit=%d", req.Limit)
	} else {
		return false, ""
	}
}

func (req *GetDomainsRequest) marker() (bool, string) {
	if req.Marker == "" {
		return false, ""
	}
	return true, fmt.Sprintf("marker=%s", req.Marker)
}

func (req *GetDomainsRequest) includes() (bool, string) {
	if req.Includes == nil {
		return false, ""
	} else if len(req.Includes) == 0 {
		return false, ""
	} else {
		var ss []string
		for _, s := range req.Includes {
			ss = append(ss, s.String())
		}
		return true, fmt.Sprintf("includes=%s", strings.Join(ss, ","))
	}
}

func (req *GetDomainsRequest) modifiedDate() (bool, string) {
	if req.ModifiedDate == "" {
		return false, ""
	}
	return true, fmt.Sprintf("modifiedDate=%s", req.ModifiedDate)
}

func (req *GetDomainsRequest) constructQuery() string {
	s := "/v1/domains?"

	for _, f := range []func() (bool, string){
		req.statuses,
		req.statusGroups,
		req.limit,
		req.marker,
		req.includes,
		req.modifiedDate,
	} {
		bb, ss := f()
		if bb {
			if s == "?" {
				s = s + ss
			} else {
				s = s + "&" + ss
			}
		}
	}

	if s == "/v1/domains?" {
		return "/v1/domains"
	} else {
		return s
	}
}

func (req *GetDomainsRequest) MarshalHTTPRequest(baseURL BaseURL, header ClientHeader) (*http.Request, error) {
	r, err := http.NewRequest(
		http.MethodGet,
		baseURL.String()+req.constructQuery(),
		nil,
	)
	if err != nil {
		return nil, err
	}

	for k, v := range header {
		r.Header[k] = v
	}

	if req.XShopperID != "" {
		r.Header["X-Shopper-ID"] = []string{req.XShopperID}
	}

	log.Printf("using reqest method: %s", r.Method)
	log.Printf("using request url: %s", r.URL)
	log.Printf("using request uri: %s", r.RequestURI)
	log.Printf("using request header: %+v", r.Header)
	log.Printf("using request body: %+v", r.Body)

	return r, nil
}

func NewGetDomainsRequest(opts ...GetDomainsRequestOption) (*GetDomainsRequest, error) {
	R := &GetDomainsRequest{}

	for _, opt := range opts {
		opt(R)
	}

	return R, nil
}

func (C *DomainsClient) GetDomains(opts ...GetDomainsRequestOption) (*GetDomainsResponse, error) {
	var Resp = &GetDomainsResponse{}
	var RespDomains []*GetDomainsDomain

	req, err := NewGetDomainsRequest(opts...)
	if err != nil {
		return nil, err
	}

	log.Printf("using client header: %+v", C.header())
	httpReq, err := req.MarshalHTTPRequest(C.baseURL, C.header())
	if err != nil {
		return nil, err
	}

	resp, err := C.httpClient.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &RespDomains); err != nil {
		return nil, err
	}

	Resp.Domains = RespDomains

	return Resp, nil
}
